var school = {};
	//var db = school.webdb.db;
    school.webdb = {};
    school.webdb.db = null;
	
    // Init Function
	function init() {
	  
	   school.webdb.open();
		school.webdb.createTable();
		
	}
	//Open Connection and create Database
	school.webdb.open = function() {
		var dbSize = 15 * 1024 * 1024; 					// 15MB
	    school.webdb.db = openDatabase("schooldb", "1.0", "schooldb", dbSize);
		
	};
	
	function save(){
		var db = school.webdb.db;
		
		db.transaction(function(tx) {
			//tx.executeSql("drop TABLE if exists school", [],school.webdb.onSuccess,school.webdb.onError); 
			tx.executeSql("CREATE TABLE if not exists school(id integer,schname character varying(50),address  character varying(150),city  character varying(15),state  character varying(15), email  character varying(15), mobile  character varying(10) )", [],null,school.webdb.onError);
			
			tx.executeSql('SELECT max(id)+1 as srno FROM school', [],function(tx, rs) {
		
			for (var i=0; i < rs.rows.length; i++) {
				var srno = rs.rows.item(0).srno;
				
			}
			if(srno==null){
				srno=1;
			}	
			
			var schname= document.getElementById("schname").value;
			var address= document.getElementById("address").value;
			var city= document.getElementById("city").value;
			var state= document.getElementById("state").value;
			var email= document.getElementById("email").value;
			var mobile= document.getElementById("mobile").value;
			if(schname=='' || schname==null){
				alert("Please Enter School Name !!!");
				return false;
			}
			else if(address=='' || address==null){
				alert("Please Enter Address !!!");
				return false;
				
			}
			else if(city=='' || city==null){
				alert("Please Enter City !!!");
				return false;
				
			}
			else if(state=='' || state==null){
				alert("Please Enter State !!!");
				return false;
				
			}
			else if(email=='' || email==null){
				alert("Please Enter E-mail !!!");
				return false;
				
			}
			else if(mobile=='' || mobile==null){
				alert("Please Enter Mobile !!!");
				return false;
				
			}
			else{
				tx.executeSql("INSERT INTO school(id,schname, address, city, state, email,mobile) VALUES (?, ?, ?, ?, ?,?, ?)",[srno,schname,address,city,state,email,mobile],function(tx, results) {
					document.getElementById("myapp").reset();
					$("#school_list").show();	
					getSchoolList();	
				},school.webdb.onError); 
			}
			
			
			},school.webdb.onError);	
		});
	}	
	function getSchoolList(){
		
		var db = school.webdb.db;
	    db.transaction(function(tx) {
		
		tx.executeSql("select * from school", [],function(tx, rs) {
			var rowOutput = "";
				for (var i=0; i < rs.rows.length; i++) {
					var row=rs.rows.item(i);
					$("#listdata").append('<li class="widget uib_w_8" data-uib="app_framework/listitem" data-ver="1"><a href="#" onclick="deleterecord('+row.id+')" data-transition="fade">'+row.schname+'<li>').listview('refresh');
					
				}
				//rowOutput +="</ul>";
				//alert(rowOutput);
				//todoItems.innerHTML = rowOutput;
				

                },school.webdb.onError);
		});
		
	}

		
	function searchname(name){
		
		if(name!==undefined && name!==""){
			var name = name.toUpperCase();
			var db = school.webdb.db;
			db.transaction(function(tx) {
			
			tx.executeSql("select * from school where UPPER(schname) like ? ", [name],function(tx, rs) {
				$("ul").empty();
					for (var i=0; i < rs.rows.length; i++) {
						var row=rs.rows.item(i);
						$("#listdata").append('<li class="widget uib_w_8" data-uib="app_framework/listitem" data-ver="1"><a href="#" onclick="deleterecord('+row.id+')" data-transition="fade">'+row.schname+'<li>').listview('refresh');
						
					}
				
					

					},school.webdb.onError);
			});
		}
		
	}
	
	
	function deleterecord(id){
		
		
			
			var db = school.webdb.db;
			db.transaction(function(tx) {
			
			tx.executeSql("delete from school where id= ? ", [id],function(tx, rs) {
				document.getElementById("myapp").reset();
				alert("Record Deleted Successfully");
					tx.executeSql("select * from school ", [],function(tx, rs) {
				$("ul").empty();
					for (var i=0; i < rs.rows.length; i++) {
						var row=rs.rows.item(i);
						$("#listdata").append('<li class="widget uib_w_8" data-uib="app_framework/listitem" data-ver="1"><a href="#" onclick="deleterecord('+row.id+')" data-transition="fade">'+row.schname+'<li>').listview('refresh');
						
					}
				
					

					},school.webdb.onError);

					},school.webdb.onError);
			});
		
		
	}


	